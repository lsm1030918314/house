import {request} from "../../request/index.js";

Page({
  data: {
    user:{},
    isChecked:"",
    isChecked2:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(wx.getStorageSync("cookieKey")==null||wx.getStorageSync("cookieKey")==""){
      wx.redirectTo({
        url: '../login/index',
      });
      return;
    }
    this.LoadUser();
  },
  //加载用户信息
  async LoadUser(){
    //创建header
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/customer/userinfo", method: "POST",header: header });
    if(res.data.code===200){
      if (res && res.header && res.header['Set-Cookie']) {
        wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
        this.setData({
          user:{
            username:res.data.data.username,
            name:res.data.data.name,
            sex:res.data.data.sex,
            phone:res.data.data.phone,
            identity:res.data.data.identity
          }
        })
        if(this.data.user.sex==0){
          this.setData({
            isChecked:"",
            isChecked2:"checked"
          })
        }else{
          this.setData({
            isChecked:"checked",
            isChecked2:""
          })
        }
      }
    }else{
      wx.redirectTo({
        url: '../login/index',
      })
    }
  },

  //确认修改个人信息
  formSubmit(e) {
    var that=this;
    wx.showModal({
      title: '个人信息修改',
      content: '确认要修改个人信息？',
      success: function (res) {
        if (res.confirm) {  
          that.formSubmit2(e);
        } else {   
          return;
        }
      }
    })
  },
  //确认后修改个人信息
  async formSubmit2(e){
    const user = e.detail.value;
    if(user.name==""||user.name==null||
      user.sex==""||user.sex==null||
      user.phone==""||user.phone==null||
      user.identity==""||user.identity==null){
        wx.showToast({
           title: "信息不能为空",
           icon: 'none',
           mask: true
         });
      return;
    } else if(user.phone.length<11||user.identity.length<18){
      wx.showToast({
        title: "手机号码和身份证格式错误",
        icon: 'none',
        mask: true
      });
      return;
    }
    //创建header
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/customer/update", method: "POST", data: user,header: header });
    if (res && res.header && res.header['Set-Cookie']) {
      wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
    }
    const {code,msg} = res.data;
    wx.showToast({
      title: msg,
      icon: 'none',
      mask: true
    });
    if(code===200){
      setTimeout(() => {
        // 返回上一个页面
        wx.navigateBack({
          delta: 1
        });
      }, 1000);
    }
  },
  //重置
  formReset () {
    this.LoadUser();
  }
})