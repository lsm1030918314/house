import {request} from "../../request/index.js";

Page({
  data: {
    houseObj: {},
    type:3
  },
  House:{
    id:0
  },
  onShow: function () {
    let pages = getCurrentPages();
    let currentPage = pages[pages.length - 1];
    let options = currentPage.options;
    const {id}=options;
    this.getNewDetail(id);
  },
  // 获取房屋详情数据
  async getNewDetail(id) {
    this.House.id=id
    const res = await request({ url: "/user/house/queryHouseId", data: this.House });
    this.setData({
      houseObj: res.data.data
    })
  },
  buy(e){
    var that=this;
    wx.showModal({
      title: '确认购买',
      content: '确认要购买这个房屋吗？',
      success: function (res) {
        if (res.confirm) {  
          that.buy2(e);
        } else {   
          return;
        }
      }
    })
  },
  async buy2(e){
    const {id} = e.currentTarget.dataset;
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/order/addOrder", method: "POST" ,data:{hId:id} ,header: header });
    const {code,msg} = res.data;
    const oid = res.data.data;
    wx.showToast({
      title: msg,
      icon: 'none',
      mask: true
    });
    if (res && res.header && res.header['Set-Cookie']) {
      wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
    }
    if(code==200){
      setTimeout(() => {
        wx.redirectTo({
          url: '../order/index?id='+oid,
        })
      }, 1000);
    }
    else if(code==500){
      wx.reLaunch({
        url: '../login/index',
      })
    }
  },
  //拨打电话
  Tel: function (e) {
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.phone // 仅为示例，并非真实的电话号码
    })
  }
})