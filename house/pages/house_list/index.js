import {request} from "../../request/index.js";

Page({
  data: {
    tabs: [
      {
        id: 0,
        value: "新房",
        isActive: true
      },
      {
        id: 1,
        value: "二手房",
        isActive: false
      },
      {
        id: 2,
        value: "租房",
        isActive: false
      }
    ],
    houseList:[]
  },
 // 接口要的参数
  QueryParams:{
    cId:"",
    type:"",
    page:1,
    limit:10
  },
  // 总页数
  totalPages:1,
  onShow(options) {
    this.setData({
      // 拼接了数组
      houseList:[]
    });
    // 1 获取当前的小程序的页面栈-数组 长度最大是10页面 
    let pages = getCurrentPages();
    // 2 数组中 索引最大的页面就是当前页面
    let currentPage = pages[pages.length - 1];
    // 3 获取url上的type参数
    const { type } = currentPage.options;
    // // 4 激活选中页面标题 当 type=1 index=0 
    this.QueryParams.cId=currentPage.options.cId||"";
    this.changeTitleByIndex(type-1);
    this.geHouseList(type);
  },

  // 获取房源列表数据
  async geHouseList(type){
    this.QueryParams.type=type;
    const res=await request({url:"/user/house/allHouse",data:this.QueryParams});
    // 获取 总条数
    const total=res.data.count;
    // 计算总页数
    this.totalPages=Math.ceil(total/this.QueryParams.limit);
    this.setData({
      // 拼接了数组
      houseList:[...this.data.houseList,...res.data.data] 
    })
  },
  // 根据标题索引来激活选中 标题数组
  changeTitleByIndex(index) {
    // 2 修改源数组
    let { tabs } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    // 3 赋值到data中
    this.setData({
      tabs
    })
  },
  // 标题点击事件 从子组件传递过来
  handleTabsItemChange(e){
    this.QueryParams.page=1;
    this.QueryParams.limit=10;
    this.setData({
      // 拼接了数组
      houseList:[]
    });
    // 1 获取被点击的标题索引
    const { index } = e.detail;
    this.changeTitleByIndex(index);
    // 2 重新发送请求 type=1 index=0
    this.geHouseList(index+1);
  },
  // 页面上滑 滚动条触底事件
  onReachBottom(){
  //  1 判断还有没有下一页数据
    if(this.QueryParams.page>=this.totalPages){
      // 没有下一页数据
      wx.showToast({ title: '没有下一页数据' });
        
    }else{
      // 还有下一页数据
      this.QueryParams.page++;
      const {type} = this.QueryParams;
      this.geHouseList(type);
    }
  }
})