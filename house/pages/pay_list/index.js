import {request} from "../../request/index.js";

Page({
  data: {
    orderList:[]
  },

  onShow: function (options) {
    if(wx.getStorageSync("cookieKey")==null||wx.getStorageSync("cookieKey")==""){
      wx.reLaunch({
        url: '../login/index',
      });
      return;
    }
    this.loadOrder();
  },
  async loadOrder(){
    //创建header
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/order/allOrderByBid", method: "POST" ,header: header });
    if (res && res.header && res.header['Set-Cookie']) {
      wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
    }
    const {code} = res.data;
    if(code===200){
      this.setData({
        orderList:res.data.data
      })
    }else{
      wx.reLaunch({
        url: '../login/index',
      })
    }
  },
  cancel(e){
    var that=this;
    wx.showModal({
      title: '取消订单',
      content: '确认要取消这个订单吗？',
      success: function (res) {
        if (res.confirm) {  
          that.cancel2(e);
        } else {   
          return;
        }
      }
    })
  },
  async cancel2(e){
    const {id} = e.currentTarget.dataset;
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/order/cancelOrder", method: "POST" ,data:{id:id} ,header: header });
    if (res && res.header && res.header['Set-Cookie']) {
      wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
    }
    const {code,msg} = res.data;
    wx.showToast({
      title: msg,
      icon: 'none',
      mask: true
    });
    if(code==200){
      this.onLoad();
    }else if(code==501){
      wx.reLaunch({
        url: '../login/index',
      })
    }
  },
  pay(e){
    wx.navigateTo({
      url: '../../pages/order/index?id='+e.currentTarget.dataset.id,
    })
  },
})