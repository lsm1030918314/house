package com.five.controller;

import com.five.common.DataGridView;
import com.five.domain.BaseEntity;
import com.five.domain.Entrust;
import com.five.domain.House;
import com.five.domain.SysUser;
import com.five.service.EntrustService;
import com.five.service.HouseService;
import com.five.service.OrderService;
import com.five.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/analysis")
public class AnalysisController {
    @Autowired
    UserService userService;
    @Autowired
    HouseService houseService;
    @Autowired
    EntrustService entrustService;
    @Autowired
    OrderService orderService;

    @RequestMapping("/LoadStaffCity")
    @PreAuthorize("hasPermission('/admin/analysis/toCityStaffAnalysis',null)")
    public Map<String, Object> LoadStaffCity(Integer city){
        List<BaseEntity> entities = this.orderService.LoadStaffCity(city);
        Map<String, Object> map = new HashMap<>();
        List<String> names = new ArrayList<>();
        List<Double> values = new ArrayList<>();
        for (BaseEntity baseEntity : entities) {
            names.add(baseEntity.getName());
            values.add(baseEntity.getValue());
        }
        map.put("name", names);
        map.put("value", values);
        return map;
    }

    //销售额统计
    @RequestMapping("/loadCityYear")
    @PreAuthorize("hasPermission('/admin/analysis/toCityAnalysis',null)")
    public List<Double> loadCityYear(Integer year,Integer city){
        List<Double> entities = orderService.loadCityYear(year,city);
        for (int i = 0; i < entities.size(); i++) {
            if (null==entities.get(i)) {
                entities.set(i, 0.0);
            }
        }
        return entities;
    }

    //员工统计
    @RequestMapping("/loadStaffYear")
    @PreAuthorize("hasPermission('/admin/analysis/toStaffAnalysis',null)")
    public List<Double> loadStaffYear(Integer year,String staff){
        List<Double> entities = orderService.loadStaffYear(year,staff);
        for (int i = 0; i < entities.size(); i++) {
            if (null==entities.get(i)) {
                entities.set(i, 0.0);
            }
        }
        return entities;
    }

    //客户数量
    @RequestMapping("/countCustomer")
    @PreAuthorize("hasPermission('/admin/analysis/toOtherAnalysis',null)")
    public DataGridView countCustomer(SysUser sysUser){
        sysUser.setAvailable(1);
        sysUser.setType(0);
        return userService.countUser(sysUser);
    }

    //员工数量
    @RequestMapping("/countStaff")
    @PreAuthorize("hasPermission('/admin/analysis/toOtherAnalysis',null)")
    public DataGridView countStaff(SysUser sysUser){
        sysUser.setAvailable(1);
        sysUser.setType(1);
        return userService.countUser(sysUser);
    }

    //房屋数量
    @RequestMapping("/houseNumber")
    @PreAuthorize("hasPermission('/admin/analysis/toOtherAnalysis',null)")
    public DataGridView HouseNumber(House house){
        house.setStatus(3);
        return houseService.queryHouseNumber(house);
    }

    //委托数量
    @RequestMapping("/entrustNumber")
    @PreAuthorize("hasPermission('/admin/analysis/toOtherAnalysis',null)")
    public DataGridView entrustNumber(Entrust entrust){
        return entrustService.queryEntrustNumber(entrust);
    }

    //销售额统计
    @RequestMapping("/loadNewOrder")
    @PreAuthorize("hasPermission('/admin/analysis/toOtherAnalysis',null)")
    public List<Double> loadNewOrder(Integer year){
        List<Double> entities = orderService.loadNewOrder(year);
        for (int i = 0; i < entities.size(); i++) {
            if (null==entities.get(i)) {
                entities.set(i, 0.0);
            }
        }
        return entities;
    }
}
