package com.five.common.file;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;

@Configuration
@EnableScheduling
public class RecyleTempFileTask {
    //每隔60分钟删除
    @Scheduled(cron = "* */60 * * * ?")
    public void recyleTempFile() {
        File file = new File(AppFileUtils.PATH);
        deleteFile(file);
    }
    /*
     * 删除图片
     */
    private void deleteFile(File file) {

        if (null != file) {
            File[] listFiles = file.listFiles();
            if (null != listFiles && listFiles.length > 0) {
                for (File f : listFiles) {
                    if (f.isFile()) {
                        if (f.getName().endsWith(SysConstast.FILE_UPLOAD_TEMP)) {
                            f.delete();
                        }
                    } else {
                        //如果是文件夹再递归
                        deleteFile(f);
                    }
                }
            }
        }
    }
}
